import { Typography } from '@material-ui/core';
import { useRouter } from 'next/router'
import React, { useContext } from 'react'
import Layout from '../components/Layout'
import { Store } from '../utils/Store';

const Shipping = () => {
    const router = useRouter();
    const { state, dispatch } = useContext(Store);
    const { userInfo } = state;
    if (!userInfo) {
        router.push('/login?redirect=/shipping')
    }
    return (
        <div>
            <Layout title="Shipping page">
                <Typography variant="h1">Shipping</Typography>
            </Layout>
        </div>
    )
}

export default Shipping
