import React, { useContext } from 'react'
import { useRouter } from 'next/router'
import NextLink from 'next/link'
import Layout from '../../components/Layout'
import Image from 'next/image'
import { Link, Button, Grid, Typography, List, ListItem, Card } from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import useStyles from '../../styles/styles'
import db from '../../utils/db'
import Product from '../../models/Product'
import axios from 'axios'
import { Store } from '../../utils/Store'

const ProductDetail = ({ product }) => {
    const {state, dispatch} = useContext(Store)
    const classes = useStyles()
    const router = useRouter();
    // const { slug } = router.query;
    // const product = data.products.find(p => p.slug === slug);

    if (!product) {
        return <div>Product not Found.</div>
    }

    const addToCartHandler = async () => {
        const existItem = state.cart.cartItems.find(x => x._id===product._id);
        const quantity = existItem ? existItem.quantity+1 :1
        const { data } = await axios.get(`/api/products/${product._id}`);
        if (data.countStock<0) {
            window.alert("Sorry, Product is out of stock.");
            return;
        }
        dispatch({type:'ADD_TO_CART', payload:{...product, quantity}});
        router.push('/cart')
    }

    return (
        <Layout title={product.name} description={product.description}>
            <section className={classes.productDetail}>
                <NextLink href="/" passHref>
                    <Link className={classes.nextLink} >
                        <Button variant="contained" color="secondary">
                            <ArrowBackIcon /> Back to products
                        </Button>
                    </Link>
                </NextLink>
                <Grid container spacing={1} className={classes.productContainer}>
                    <Grid item md={6} xs={12}>
                        <Image
                            src={product.image}
                            alt={product.name}
                            width={500}
                            height={500}
                            layout="responsive"
                        ></Image>
                    </Grid>
                    <Grid item md={3} xs={12}>
                        <List>
                            <ListItem>
                                <Typography component="h4" variant="h4">
                                    {product.name}
                                </Typography>
                            </ListItem>
                            <ListItem>
                                <Typography>Category: {product.category}</Typography>
                            </ListItem>
                            <ListItem>
                                <Typography>Brand: {product.brand}</Typography>
                            </ListItem>
                            <ListItem>
                                <Typography>Rating: {product.rating} stars ({product.numReviews} reviews)</Typography>
                            </ListItem>
                            <ListItem>
                                <Typography> Description: {product.description}</Typography>
                            </ListItem>
                        </List>
                    </Grid>
                    <Grid item md={3} xs={12}>
                        <Card>
                            <List>
                                <ListItem>
                                    <Grid container>
                                        <Grid item xs={6}>
                                            <Typography>Price</Typography>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Typography>${product.price}</Typography>
                                        </Grid>
                                    </Grid>
                                </ListItem>
                                <ListItem>
                                    <Grid container>
                                        <Grid item xs={6}>
                                            <Typography>Status</Typography>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Typography>
                                                {product.countInStock > 0 ? 'In stock' : 'Unavailable'}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </ListItem>
                                <ListItem>
                                    <Button
                                        fullWidth
                                        variant="contained"
                                        color="primary"
                                        onClick={addToCartHandler}
                                    >
                                        Add to cart
                                    </Button>
                                </ListItem>
                            </List>
                        </Card>
                    </Grid>
                </Grid>
            </section>
        </Layout>
    )
}

export default ProductDetail;


export async function getServerSideProps(context) {
    const { params } = context;
    const { slug } = params
    await db.connect();
    const product = await Product.findOne({ slug }).lean()
    await db.disconnect();
    return {
        props: {
            product: db.convertDocToObj(product)
        }
    }
}
