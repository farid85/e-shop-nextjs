import Layout from '../components/Layout'
import Main from '../components/Main'
import db from '../utils/db';
import Product from '../models/Product';


export default function Home(props) {
  const {products} = props;
  return (
    <Layout>
      <div>
        <Main data={products} />
      </div>
    </Layout>

  )
}


export async function getServerSideProps() {
  await db.connect();
  const products = await Product.find({}).lean()
  await db.disconnect();
  return {
    props:{ 
      products: products.map(db.convertDocToObj)
    }
  }
}