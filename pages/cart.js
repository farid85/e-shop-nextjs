import React, { useContext } from 'react'
import NextLink from 'next/link'
import dynamic from 'next/dynamic'
import Image from 'next/image'
import {
    Grid,
    Typography,
    Button,
    TableContainer,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Link,
    Select,
    MenuItem,
    Card,
    List,
    ListItem,
} from '@material-ui/core'
import Layout from '../components/Layout'
import { Store } from '../utils/Store'
import axios from 'axios'
import { useRouter } from 'next/router'

const cart = () => {
    const router = useRouter()
    const { state, dispatch } = useContext(Store)
    const { cart } = state
    console.log(cart)

    const updateCartHandler = async (item, quantity) => {
        const { data } = await axios.get(`/api/products/${item._id}`);
        if (data.countStock < quantity) {
            window.alert("Sorry, Product is out of stock.");
            return;
        }
        dispatch({ type: 'ADD_TO_CART', payload: { ...item, quantity } });
    }

    const removeItemHandler = (item) => {
        dispatch({ type: 'REMOVE_FROM_CART', payload: item });
    }

    const checkoutHandler = () => {
        router.push('/shipping')
    }


    return (
        <Layout title="Cart Page">
            <section>
                <Typography variant="h2">Shopping Cart</Typography>
                {cart.cartItems.length === 0 ? (
                    <div>Cart is empty. <NextLink href="/" passHref><Link variant="contained" color="secondary">Go to shopping</Link></NextLink></div>
                ) :
                    (
                        <Grid container spacing={3}>
                            <Grid item md={9} xs={12}>
                                <TableContainer>
                                    <Table>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Image</TableCell>
                                                <TableCell>Name</TableCell>
                                                <TableCell align="right">Quantity</TableCell>
                                                <TableCell align="right">Price</TableCell>
                                                <TableCell align="right">Action</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {cart.cartItems.map((item) => (
                                                <TableRow key={item._id}>
                                                    <TableCell>
                                                        <NextLink href={`/product/${item.slug}`} passHref>
                                                            <Link>
                                                                <Image
                                                                    src={item.image}
                                                                    alt={item.name}
                                                                    width={50}
                                                                    height={50}
                                                                >
                                                                </Image>
                                                            </Link>
                                                        </NextLink>
                                                    </TableCell>
                                                    <TableCell>
                                                        <NextLink href={`/product/${item.slug}`} passHref>
                                                            <Link>
                                                                <Typography color="secondary">{item.name}</Typography>
                                                            </Link>
                                                        </NextLink>
                                                    </TableCell>
                                                    <TableCell align="right">
                                                        <Select
                                                            value={item.quantity}
                                                            onChange={(e) => updateCartHandler(item, e.target.value)}
                                                        >
                                                            {[...Array(item.countInStock).keys()].map(x => (
                                                                <MenuItem key={x + 1} value={x + 1}>
                                                                    {x + 1}
                                                                </MenuItem>
                                                            ))}
                                                        </Select>
                                                    </TableCell>
                                                    <TableCell align="right">${item.price}</TableCell>
                                                    <TableCell align="right">
                                                        <Button
                                                            variant="contained"
                                                            color="secondary"
                                                            onClick={() => removeItemHandler(item)}
                                                        >
                                                            X
                                                        </Button>
                                                    </TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Grid>
                            <Grid item md={3} xs={12}>
                                <Card>
                                    <List>
                                        <ListItem>
                                            <Typography variant="h3">
                                                Subtotal ({cart.cartItems.reduce((a, c) => a + c.quantity, 0)}{' '}
                                                items) : $
                                                {cart.cartItems.reduce((a, c) => a + c.quantity * c.price, 0)}
                                            </Typography>
                                        </ListItem>
                                        <ListItem>
                                            <Button
                                                variant="contained"
                                                color="primary"
                                                fullWidth
                                                onClick={checkoutHandler}
                                            >
                                                Check Out
                                            </Button>
                                        </ListItem>
                                    </List>
                                </Card>
                            </Grid>
                        </Grid>
                    )}
            </section>
        </Layout>
    )
}

export default dynamic(() => Promise.resolve(cart), { ssr: false });
