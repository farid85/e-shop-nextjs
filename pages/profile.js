import { Typography } from '@material-ui/core';
import { useRouter } from 'next/router'
import React, { useContext } from 'react'
import Layout from '../components/Layout'
import { Store } from '../utils/Store';

const Profile = () => {
    const router = useRouter();
    const { state, dispatch } = useContext(Store);
    const { userInfo } = state;
    return (
        <div>
            <Layout title="Profile page">
                <Typography variant="h1">Profile</Typography>
                <Typography variant="h2">{userInfo.name}</Typography>
                <Typography variant="h2">{userInfo.email}</Typography>
            </Layout>
        </div>
    )
}

export default Profile
