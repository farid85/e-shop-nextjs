import { makeStyles } from '@material-ui/core/styles';
import { deepPurple } from '@material-ui/core/colors';

const useStyles = makeStyles((theme) => ({
    topWrapper: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
    },
    navbar: {
        background: '#bc4e9c',
        background: '-webkit-linear-gradient(to right, #f80759, #bc4e9c)',
        background: 'linear-gradient(to right, #f80759, #bc4e9c)',
        '& a': {
            color: 'white',
            marginLeft: 10
        }
    },
    main: {
        flexGrow: 1
    },
    grow: {
        flexGrow: 1
    },
    wrapper: {
        display: 'flex',
        alignItems: 'center',
    },
    icon: {
        fontSize: 18,
        marginRight: 5
    },
    cart: {
        display: 'flex',
        alignItems: 'center',
    },
    footer: {
        padding: 20,
        textAlign: 'center',
        background: '#bc4e9c',
        background: '-webkit-linear-gradient(to right, #f80759, #bc4e9c)',
        background: 'linear-gradient(to right, #f80759, #bc4e9c)',
        color: 'white',
        marginTop: 20
    },
    productDetail: {
        marginTop: 15,
    },
    nextLink: {
        textDecoration: 'none',
        '&:hover': {
            textDecoration: 'none',
        },
    },
    productContainer: {
        marginTop: 10,
    },
    form: {
        width: '100%',
        maxWidth: 800,
        margin: '0 auto',
    },
    navbarButton: {
        color: '#ffffff',
        textTransform: 'initial',
    },
    purple: {
        color: theme.palette.getContrastText(deepPurple[500]),
        backgroundColor: deepPurple[500],
        margin:'0 7px',
        cursor: 'pointer'
      },
}));



export default useStyles;