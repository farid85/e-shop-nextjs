import React, { useContext } from 'react'
import { Grid, Card, CardActionArea, CardMedia, CardContent, Typography, CardActions, Button } from '@material-ui/core'
import NextLink from 'next/link'
import router, { useRouter } from 'next/router'
import axios from 'axios'
import { Store } from '../utils/Store'


const Home = ({data}) => {
    const {state, dispatch} = useContext(Store)
    const router = useRouter();
    const addToCartHandler = async (product) => {
        const existItem = state.cart.cartItems.find(x => x._id===product._id);
        const quantity = existItem ? existItem.quantity+1 :1
        const { data } = await axios.get(`/api/products/${product._id}`);
        if (data.countStock<0) {
            window.alert("Sorry, Product is out of stock.");
            return;
        }
        dispatch({type:'ADD_TO_CART', payload:{...product, quantity}});
        router.push('/cart')
    }
    return (
        <div>
            <h1>Products</h1>
            <Grid container spacing={3}>
                {data.map(product => (
                    <Grid item md={4} key={product.name}>
                        <Card>
                            <NextLink href={`/product/${product.slug}`} passHref>
                            <CardActionArea>
                                <CardMedia
                                    height={300}
                                    component="img"
                                    image={product.image}
                                    title={product.name}
                                >
                                </CardMedia>
                                <CardContent>
                                    <Typography>{product.name}</Typography>
                                </CardContent>
                            </CardActionArea>
                            </NextLink>
                            <CardActions>
                                <Typography>${product.price}</Typography>
                                <Button color="primary" onClick={()=> addToCartHandler(product)}>Add to cart</Button>
                            </CardActions>
                        </Card>
                    </Grid>
                ))}
            </Grid>
        </div>
    )
}

export default Home
