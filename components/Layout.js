import React, { useContext, useState } from 'react'
import Head from 'next/head'
import NextLink from 'next/link'
import { useRouter } from 'next/router'
import {
    AppBar,
    Toolbar,
    Container,
    Typography,
    Link,
    createTheme,
    ThemeProvider,
    CssBaseline,
    Switch,
    Badge,
    Avatar,
    Button, Menu, MenuItem
} from '@material-ui/core'
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import useStyles from '../styles/styles'
import { Store } from '../utils/Store';
import Cookies from 'js-cookie'


const Layout = ({ title, description, children }) => {
    const router = useRouter()
    const { state, dispatch } = useContext(Store)
    const { darkMode, cart, userInfo } = state
    const theme = createTheme({
        typography: {
            h1: {
                fontSize: '1.6rem',
                fontWeight: 400,
                margin: '1rem 0',
            },
            h2: {
                fontSize: '1.6rem',
                fontWeight: 400,
                margin: '1rem 0',
            },
            h3: {
                fontSize: '1.2rem',
                fontWeight: 400,
                margin: '1rem 0',
            }
        },
        palette: {
            type: darkMode ? 'dark' : 'light',
            primary: {
                main: '#f0c000'
            },
            secondary: {
                main: '#bc4e9c'
            },
        }
    })
    const classes = useStyles();

    const darkModeChangeHandler = () => {
        dispatch({ type: darkMode ? 'DARK_MODE_OFF' : 'DARK_MODE_ON' });
        const newDarkMode = !darkMode;
        Cookies.set('darkMode', newDarkMode ? 'ON' : 'OFF', { expires: 7 })  // expires 7 days from now
    }


    const [anchorEl, setAnchorEl] = useState(null);

    const loginClickHandler = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const loginMenuCloseHandler = () => {
        setAnchorEl(null);
    };
    const logoutClickHandler = () => {
        setAnchorEl(null);
        dispatch({ type: 'LOGOUT_USER' });
        Cookies.remove('userInfo');
        Cookies.remove('cartItems');
        router.push('/')
    }

    // let firstNameCheck;
    // let lastNameCheck

    // console.log(userInfo)
    // if (userInfo) {
    //     const words = userInfo.name.split(' ');
    //     if (words[0]) {
    //         firstNameCheck = words[0].charAt(0)
    //     }
    //     if (words[1]) {
    //         lastNameCheck = words[1].charAt(0)
    //     }
    // }


    return (
        <div className={classes.topWrapper}>
            <Head>
                <title>{title ? `${title} - E-shop` : 'E-shop next app'}</title>
                {description && <meta name="description" content={description} />}
            </Head>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <AppBar position="static" className={classes.navbar}>
                    <Toolbar>
                        <NextLink href="/" passHref>
                            <Link>
                                <Typography>
                                    E-Shop
                                </Typography>
                            </Link>
                        </NextLink>
                        <div className={classes.grow}></div>
                        <div className={classes.wrapper}>
                            <Switch checked={darkMode} onChange={darkModeChangeHandler}></Switch>
                            <NextLink href="/cart" passHref >
                                <Link className={classes.cart}>
                                    {cart.cartItems.length > 0 ? (
                                        <Badge color="primary" badgeContent={cart.cartItems.length}>
                                            <ShoppingCartOutlinedIcon className={classes.icon} /> <Typography>Cart</Typography>
                                        </Badge>
                                    ) : 'Cart'}

                                </Link>
                            </NextLink>
                            {userInfo ?
                                <>
                                    {/* <Avatar
                                        className={classes.purple}
                                        aria-controls="simple-menu"
                                        aria-haspopup="true"
                                        onClick={loginClickHandler}
                                    >
                                        {firstNameCheck && firstNameCheck}{lastNameCheck && lastNameCheck} {!firstNameCheck && !lastNameCheck && 'DU'}
                                    </Avatar> */}
                                    <Button
                                        aria-controls="simple-menu"
                                        aria-haspopup="true"
                                        onClick={loginClickHandler}
                                    >
                                        {userInfo.name}
                                    </Button>
                                    <Menu
                                        id="simple-menu"
                                        anchorEl={anchorEl}
                                        keepMounted
                                        open={Boolean(anchorEl)}
                                        onClose={loginMenuCloseHandler}
                                    >
                                        <MenuItem><NextLink href="/profile">Profile</NextLink></MenuItem>
                                        <MenuItem onClick={loginMenuCloseHandler}>My account</MenuItem>
                                        <MenuItem onClick={logoutClickHandler}>Logout</MenuItem>
                                    </Menu>
                                </>
                                :
                                <NextLink href="/login" passHref>
                                    <Link>Login</Link>
                                </NextLink>
                            }

                        </div>
                    </Toolbar>
                </AppBar>

                <Container className={classes.main}>
                    {children}
                </Container>
                <footer className={classes.footer}>
                    All rights reserved. E-shop is licensed 2021.
                </footer>
            </ThemeProvider>


        </div>
    )
}

export default Layout
